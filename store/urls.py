from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),  # This is just an example, adjust as needed
    path('signup/', views.signup, name='signup'),  

]
